**'FFTJava' specifications and project description**

---
								Contributor: Grzegorz Rybak M.Sc. Eng.
								Affiliation: Institute of Applied Computer Science Lodz University of Technology, POLAND
---

The aim of the project: 

Provide a solution of STFT computation for tomographic data processing (ECT - Electrical Capacitance Tomography).
There are several libraries already available for FFT computation in JAVA but still some optimization methods may be applied.
This work is done in scope of PhD work and this is a consecutive approach to investigate efficiency of available techniques.

Solutions under the investigation: JTransform, FFTW, the iterative in-siu solution from Columbia University, 
original solution based on recursive approach, original solution based on metaprogramming techniques.

## Usage

1. Download (clone) repository.
2. Build project: clean install.
3. Run created jar file (currently there is no release, yet.).
---
		java -jar ../../../../target/fftjava-1.0-SNAPSHOT-jar-with-dependencies.jar -warmup 0 -iterations 1 -alg 2 -overlap 1 -stft_size 1 -tWindow 128
---	

 *Program arguments*:
 
---
    -combinations   : Use combination mechanism

    -printResult    : Show last FFT result

 	-alg N          : Choose algorithm group
 
  	-iterations N   : How many iterations should be done. The time output is an average. (default: 35)
 
  	-overlap N      : STFT overlap coefficient (default: 1)
 
  	-stft_size N    : STFT input array size for one iteration (default: 1000)
 
 	-tWindow N      : The size of time window (default: 128)
 
  	-warmup N       : Remove execution time of first iterations (default: 0)
--- 
 
*It is recommend to use run scripts (Windows platform only): fftjava\src\main\resources\examples\stft_one_line.bat.*