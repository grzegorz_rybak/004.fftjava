package pl.rybakgrzegorz.fisisto.fftjava;

import org.jtransforms.fft.DoubleFFT_1D;
import org.junit.Test;
import pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.FFT_META_GENERATED;
import pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.FFT_META_GENERATED_STOCKHAM;

import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * Created by T1 on 2020-10-09.
 */
public class FastTest {
    @Test
    public void test() throws InterruptedException {
        double[] array = IntStream.range(0, 80).mapToDouble(x -> x).toArray();
        int windowSize = 8;
        pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.generated_bitrev.FFT_META_GENERATEDI
                fft_meta_generated = FFT_META_GENERATED.createExecutorSupplier(windowSize);
        pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.generated_stock.FFT_META_GENERATEDI
                fft_meta_generated_stock = FFT_META_GENERATED_STOCKHAM.createExecutorSupplier(windowSize);

        DoubleFFT_1D jTransform = new DoubleFFT_1D(windowSize);

        for (int i = 0; i < 2; i++) {
            double[] array2 = BenchmarkUtils.cutTo1D(array, 8, i);
            fft_meta_generated.executor.accept(array2);
            System.out.println(Arrays.toString(array2));

            double[] array3 = BenchmarkUtils.cutTo1D(array, 8, i);
            fft_meta_generated_stock.executor.accept(array3);
            System.out.println(Arrays.toString(array3));

            double[] array4 = BenchmarkUtils.cutTo1D(array, 8, i);
            jTransform.complexForward(array4);
            System.out.println("Jt: " + Arrays.toString(array4));
            System.out.println("NEXT iteration:-----------------------------------------------------------------");
        }

    }

}
