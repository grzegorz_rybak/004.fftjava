package pl.rybakgrzegorz.fisisto.fftjava;

final class BenchmarkUtils {

    public static double[] cutTo1D(final BenchmarkState benchmarkState, final int startIndex) {
        final int start = startIndex * 2;
        double[] out = new double[benchmarkState.windowSize * 2];
        for (int i = start; i < start + benchmarkState.windowSize * 2; i += 2) {
            out[i - start] = benchmarkState.dataExtend[i];
        }
        return out;
    }

    public static double[] cutTo1D(final double [] dataExtend, final int windowSize, final int startIndex) {
        final int start = startIndex * 2;
        double[] out = new double[windowSize * 2];
        for (int i = start; i < start + windowSize * 2; i += 2) {
            out[i - start] = dataExtend[i];
        }
        return out;
    }

    public static double[] convertToComplexInput(double[] data) {
        final double[] dataExtend = new double[data.length * 2];
        for (int i = 0; i < data.length; i++) {
            dataExtend[i * 2] = data[i];
            dataExtend[i * 2 + 1] = 0;
        }
        return dataExtend;
    }
}
