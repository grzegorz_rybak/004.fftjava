package pl.rybakgrzegorz.fisisto.fftjava;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.RunnerException;

public class Benchmarks {

    public static void main(final String[] args) throws IOException, RunnerException {
        org.openjdk.jmh.Main.main(new String[]{});
    }

    @Group(value = "FFT")
    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @Fork(value = 0)
    @Warmup(iterations = 5)
    @Measurement(iterations = 30)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    public int metaProgramGeneratedFFTStock(final BenchmarkState benchmarkState) throws InterruptedException {
        for (int i = 0; i < benchmarkState.iterations; i++) {
            benchmarkState.fft_meta_generated_stock.executor.accept(BenchmarkUtils.cutTo1D(benchmarkState, i));
        }
        return 0;
    }

    @Group(value = "FFT")
    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @Fork(value = 0)
    @Warmup(iterations = 5)
    @Measurement(iterations = 30)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    public int jTransform(final BenchmarkState benchmarkState) throws InterruptedException {
        for (int i = 0; i < benchmarkState.iterations; i++) {
            benchmarkState.jTransform.complexForward(BenchmarkUtils.cutTo1D(benchmarkState, i));
        }
        return 0;
    }

    @Group(value = "FFT")
    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @Fork(value = 0)
    @Warmup(iterations = 5)
    @Measurement(iterations = 30)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    public int metaProgramGeneratedFFT(final BenchmarkState benchmarkState) throws InterruptedException {
        for (int i = 0; i < benchmarkState.iterations; i++) {
            benchmarkState.fft_meta_generated.executor.accept(BenchmarkUtils.cutTo1D(benchmarkState, i));
        }
        return 0;
    }

}
