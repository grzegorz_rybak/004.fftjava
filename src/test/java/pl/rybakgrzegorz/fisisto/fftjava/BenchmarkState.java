package pl.rybakgrzegorz.fisisto.fftjava;

import org.jtransforms.fft.DoubleFFT_1D;
import org.openjdk.jmh.annotations.*;
import pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.FFT_META_GENERATED;

import pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.FFT_META_GENERATED_STOCKHAM;
import pl.rybakgrzegorz.fisisto.fftjava.demo.demomodel.InputDataGenerator;

@State(Scope.Group)
public class BenchmarkState {

    public DoubleFFT_1D jTransform;

    public pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.generated_bitrev.FFT_META_GENERATEDI fft_meta_generated;

    public pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.generated_stock.FFT_META_GENERATEDI fft_meta_generated_stock;

    public int windowSize = 16;

    public int iterations = 200000;

    public double[] dataExtend;


    @Setup(Level.Trial)
    public void doSetup() {
        dataExtend = BenchmarkUtils
                .convertToComplexInput(InputDataGenerator.GENERATE_ARRAY_STRATEGY.apply(windowSize + iterations));

        jTransform = new DoubleFFT_1D(windowSize);
        fft_meta_generated = FFT_META_GENERATED.createExecutorSupplier(windowSize);
        fft_meta_generated_stock = FFT_META_GENERATED_STOCKHAM.createExecutorSupplier(windowSize);
    }

    @Setup(Level.Invocation)
    public void doSetup2() {
    }

    @TearDown(Level.Trial)
    public void doTearDown() {
    }

}
