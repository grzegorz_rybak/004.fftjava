package pl.rybakgrzegorz.fisisto.fftjava;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.rybakgrzegorz.fisisto.fftjava.combinations.CombinationExecutor;
import pl.rybakgrzegorz.fisisto.fftjava.demo.Execution;
import pl.rybakgrzegorz.fisisto.fftjava.demo.ExecutionAccuracyAnalyzer;
import pl.rybakgrzegorz.fisisto.fftjava.demo.demomodel.ExecutionBundle;
import pl.rybakgrzegorz.fisisto.fftjava.demo.demomodel.ExecutionOutput;
import pl.rybakgrzegorz.fisisto.fftjava.demo.demomodel.DemoOutput;
import pl.rybakgrzegorz.fisisto.fftjava.demo.demomodel.DemoRunnerArgs;

import java.io.StringWriter;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class DemoRunner {

    private static final Logger LOGGER = LoggerFactory.getLogger(DemoRunner.class);
    public static final double NANO_TO_MILLI_SEC = 1e6;

    public static void main(final String[] args) {
        LOGGER.info("Start java fft demo");
        new DemoRunner().demoRunnerInnerMethod(args);
    }

    private void demoRunnerInnerMethod(final String[] args) {
        final DemoRunnerArgs demoArguments = new DemoRunnerArgs();

        final CmdLineParser parser = new CmdLineParser(demoArguments);
        try {
            parser.parseArgument(args);
            DemoRunnerArgs.validate(demoArguments);

            if (demoArguments.useCombinations) {
                performDemoWithCombinations(demoArguments);
            } else {
                final ExecutionOutput executionOutput = new ExecutionOutput();
                final ExecutionBundle executionBundle = new ExecutionBundle();
                executionBundle.executionOutput = executionOutput;
                executionBundle.demoRunnerArgs = demoArguments;
                performDemo(executionBundle);
            }
        } catch (CmdLineException e) {
            final StringWriter writer = new StringWriter();

            LOGGER.error("Cannot run java fft runner due to root cause", e);
            addUsage(writer);
            parser.printUsage(writer, null);
            LOGGER.error(writer.toString());
        }
    }

    private void performDemoWithCombinations(final DemoRunnerArgs demoArguments) {
        final CombinationExecutor combinationExecutor = new CombinationExecutor(demoArguments);
        combinationExecutor.doForEachCombination(this::performDemo);
    }

    private void addUsage(final StringWriter writer) {
        writer.append("\n\n Usage:\n");
        writer.append("\tAlgorithm group: 1): MetaFFT, JTransform, Columbia, FFTW, Recursive, DFT, SDFT\n");
        writer.append("\tAlgorithm group: 2): MetaFFT, JTransform, Columbia, FFTW, Recursive, SDFT\n");
        writer.append("\tAlgorithm group: 3): MetaFFT, JTransform, Columbia\n");
        writer.append("\tAlgorithm group: 4): MetaFFTCore, MetaFFT, JTransform, Columbia\n");
    }

    private void performDemo(final ExecutionBundle executionBundle) {
        LOGGER.debug("Perform demo with arguments: {}", executionBundle.demoRunnerArgs);

        final Execution execution = new Execution();
        IntStream.range(0, executionBundle.demoRunnerArgs.iterations).forEach((x) -> execution.runSingleIteration(executionBundle.demoRunnerArgs, executionBundle.executionOutput));

        if (executionBundle.demoRunnerArgs.printResult) {
            final ExecutionAccuracyAnalyzer executionAccuracyAnalyzer = new ExecutionAccuracyAnalyzer();
            executionAccuracyAnalyzer.analyse(executionBundle.executionOutput);
        }

        final Map.Entry<String, Double> bestAlgorithmTime = executionBundle.executionOutput.entrySet()
                .stream()
                .collect(Collectors
                        .toMap(e -> e.getKey(), e ->
                                removeFirst(executionBundle.demoRunnerArgs.warmUp, e.getValue())
                                        .stream()
                                        .mapToLong(x -> x.getTime())
                                        .average()
                                        .getAsDouble() / NANO_TO_MILLI_SEC))
                .entrySet()
                .stream()
                .filter(e -> e.getValue() != 0)
                .min(Comparator.comparing(Map.Entry::getValue)).get();

        executionBundle.executionOutput.entrySet().stream()
                .forEach((e) ->
                        LOGGER.debug(String.format("Algorithm '%-20s' time: %-15s [ms]  comparison: %-21s [-]",
                                e.getKey(), e.getValue()
                                        .stream()
                                        .mapToLong(x -> x.getTime())
                                        .average()
                                        .getAsDouble() / NANO_TO_MILLI_SEC,
                                (e.getValue()
                                        .stream()
                                        .mapToLong(x -> x.getTime())
                                        .average()
                                        .getAsDouble() / NANO_TO_MILLI_SEC) / bestAlgorithmTime.getValue())));
    }

    private static List<DemoOutput> removeFirst(int toRemove, List<DemoOutput> value) {
        for (int i = 0; i < toRemove; i++) {
            value.remove(0);
        }
        return value;
    }

}
