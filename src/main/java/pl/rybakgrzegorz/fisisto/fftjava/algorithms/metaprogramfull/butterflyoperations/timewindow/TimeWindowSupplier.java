package pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.butterflyoperations.timewindow;

import java.util.List;

public interface TimeWindowSupplier {

    List<Integer> supplyFirstTimeWindow();

    List<Integer> supplySecondTimeWindow();
}
