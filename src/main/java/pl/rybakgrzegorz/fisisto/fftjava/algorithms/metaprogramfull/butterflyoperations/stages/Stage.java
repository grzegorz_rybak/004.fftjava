package pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.butterflyoperations.stages;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import java.util.Collection;

public class Stage {

    private final Radix2ComponentAggregator radixAggregator;

    private Multimap<Integer, Radix2Component> mapFirst = ArrayListMultimap.create();

    private Multimap<Integer, Radix2Component> mapSecond = ArrayListMultimap.create();

    public Stage(final Radix2ComponentAggregator radixAggregator) {
        this.radixAggregator = radixAggregator;

        extractToStages(this.radixAggregator.radix2ComponentFirst, mapFirst);
        extractToStages(this.radixAggregator.radix2ComponentSecond, mapSecond);
    }


    private Stage extractToStages(final Radix2Component radix2Component, final Multimap<Integer, Radix2Component> map) {
        radix2Component.getOdd().ifPresent(r -> extractToStages(r, map));
        radix2Component.getEven().ifPresent(r -> extractToStages(r, map));
        map.put(computeStage(radix2Component.getFftData()), radix2Component);
        return this;
    }

    private int computeStage(final Collection collection){
        return (int) (Math.log10(collection.size())/Math.log10(2));
    }

    public Multimap<Integer, Radix2Component> getMapFirst() {
        return mapFirst;
    }

    public Multimap<Integer, Radix2Component> getMapSecond() {
        return mapSecond;
    }
}
