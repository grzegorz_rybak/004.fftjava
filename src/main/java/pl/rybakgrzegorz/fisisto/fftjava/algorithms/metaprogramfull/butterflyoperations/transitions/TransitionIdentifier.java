package pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.butterflyoperations.transitions;


import com.google.common.collect.Multimap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.butterflyoperations.stages.Radix2Component;
import pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.butterflyoperations.stages.Stage;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class TransitionIdentifier {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransitionIdentifier.class);

    private final Stage stage;

    private final TransitionsManager transitionsManager = new TransitionsManager();

    private Set<Integer> alreadyUsedIndexes = new TreeSet<>();

    public TransitionIdentifier(final Stage stage) {
        this.stage = stage;
    }

    public void doIt() {
        final int maxStageIndex = stage.getMapFirst().keySet().size() - 1;
        final Collection<Radix2Component> lastStage = stage.getMapFirst().get(maxStageIndex);
        final Radix2Component root = lastStage.iterator().next();

        final AtomicInteger iterator = new AtomicInteger(0);
        final LinkedList<Radix2Component> list = new LinkedList();
        list.add(root);
        while (iterator.get() < list.size()) {
            checkTree(maxStageIndex, list, iterator);
            iterator.addAndGet(1);
        }
    }

    private void checkTree(final int maxStageIndex, final LinkedList<Radix2Component> list, final AtomicInteger iterator) {
        final Radix2Component component = list.get(iterator.get());
        final int currentStage = maxStageIndex - component.getPathToOperation().length();

       // LOGGER.debug("Check tree node. Enter method for {}", componentKeys(component));

        final Optional<Radix2Component> match = extractTransitions(currentStage, component, this.stage.getMapSecond());
        if(match.isPresent()){
            alreadyUsedIndexes.addAll(component.getFftData().stream().map(ind -> ind.getKey()).collect(Collectors.toList()));
        }else{
            component.getOdd().filter(this::allowToProcessChildren).ifPresent(c1 -> list.add(c1));
            component.getEven().filter(this::allowToProcessChildren).ifPresent(c1 -> list.add(c1));
        }
    }

    private boolean allowToProcessChildren(final Radix2Component component) {
        if (component.getFftData().size() < 2) return false;
        final boolean allowToProcess = component.getFftData().stream().filter(entry -> alreadyUsedIndexes.contains(entry.getKey())).count() == 0;
       // if (allowToProcess) LOGGER.debug("Allows to add node {}", componentKeys(component));
        return allowToProcess;
    }

    private Optional<Radix2Component> extractTransitions(final Integer stageIndex,
                                                         final Radix2Component component,
                                                         final Multimap<Integer, Radix2Component> mapSecond) {

        final String currentComponentName = componentGeneralBufferIndexesString(component);
        final Optional<Radix2Component> match = mapSecond.get(stageIndex)
                .stream()
                .filter((component2) -> currentComponentName.equals(componentGeneralBufferIndexesString(component2))).findFirst();

        match.ifPresent(m -> {
            //LOGGER.debug("Have match [indexes of general meas. buffer]: {}", componentGeneralBufferIndexesString(m));
           // LOGGER.debug("Have match {} -> {}", componentKeys(component), componentKeys(m));
            final boolean allowToProcess1 = component.getFftData().stream().filter(entry -> alreadyUsedIndexes.contains(entry.getKey())).count() == 0;
            final boolean allowToProcess2 = m.getFftData().stream().filter(entry -> alreadyUsedIndexes.contains(entry.getKey())).count() == 0;
            if(allowToProcess1 && allowToProcess2){
         //       LOGGER.debug("Allows to process transitions: {} -> {}",componentKeys(component), componentKeys(m));
                transitionsManager.insertTransition(stageIndex,component, m);
            }else{
         //       LOGGER.debug("Block transitions due to index conflict: {} -> {}",componentKeys(component), componentKeys(m));
            }
        });

        return match;
    }

    public static String componentGeneralBufferIndexesString(final Radix2Component radix2Component) {
        return radix2Component.getFftData().stream().map(entry ->
                entry.getValue().toString()).reduce((before, next) -> before + "|" + next).get();
    }

    public static String componentKeys(final Radix2Component radix2Component) {
        return radix2Component.getFftData().stream().map(entry ->
                entry.getKey().toString()).reduce((before, next) -> before + "|" + next).get();
    }

    public TransitionsManager getTransitionsManager() {
        return transitionsManager;
    }
}
