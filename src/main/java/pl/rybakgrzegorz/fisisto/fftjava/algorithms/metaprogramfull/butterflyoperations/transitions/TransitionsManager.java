package pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.butterflyoperations.transitions;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.butterflyoperations.stages.Radix2Component;


public class TransitionsManager {


    public static class Pair {
        public Radix2Component first;
        public Radix2Component second;

        public int transitionTableStartIndex;

        public Pair(Radix2Component first, Radix2Component second) {
            this.first = first;
            this.second = second;
        }
    }

    private Multimap<Integer, Pair> map = ArrayListMultimap.create();

    public void insertTransition(Integer stageIndex, final Radix2Component first, final Radix2Component second) {
        map.put(stageIndex, new Pair(first, second));
    }

    public int getListSize() {
        return map.size();
    }

    public Multimap<Integer, Pair> getMap() {
        return map;
    }
}
