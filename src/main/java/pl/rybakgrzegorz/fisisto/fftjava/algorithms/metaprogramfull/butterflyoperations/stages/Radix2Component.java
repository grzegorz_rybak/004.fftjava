package pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.butterflyoperations.stages;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.rybakgrzegorz.fisisto.fftjava.algorithms.columbia.ColumbiaFFT;

import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Radix2Component {

    private static final Logger LOGGER = LoggerFactory.getLogger(Radix2Component.class);

    private final List<AbstractMap.SimpleEntry<Integer, Integer>> fftData;

    private final int butterflyParameter_N;

    private final Object[] butterflyParameter_Twiddle;

    private Radix2Component odd;

    private Radix2Component even;

    private final String pathToOperation;

    public Radix2Component(final List<AbstractMap.SimpleEntry<Integer, Integer>> fftData, String pathToOperation) {
        this.fftData = fftData;
        this.butterflyParameter_N = fftData.size();
        this.butterflyParameter_Twiddle = ColumbiaFFT.bitReverse(IntStream.range(0,fftData.size()/2).boxed().toArray());
        this.pathToOperation = pathToOperation;
    }

    public Radix2Component recursiveDivision() {
        this.odd = new Radix2Component(
                this.fftData.stream()
                        .filter(x -> indexOf(this.fftData, x) % 2 == 0)
                        .collect(Collectors.toList()),this.pathToOperation+"L");
        this.even = new Radix2Component(
                this.fftData.stream()
                        .filter(x -> indexOf(this.fftData, x) % 2 == 1)
                        .collect(Collectors.toList()),this.pathToOperation+"R");

        if (this.fftData.size() > 2) {
            this.odd.recursiveDivision();
            this.even.recursiveDivision();
        }

        return this;
    }

    private Integer indexOf(final List<AbstractMap.SimpleEntry<Integer, Integer>> list, final Map.Entry<Integer, Integer> element) {
        return list.indexOf(element);
    }

    public List<AbstractMap.SimpleEntry<Integer, Integer>> getFftData() {
        return fftData;
    }

    public Optional<Radix2Component> getOdd() {
        return Optional.ofNullable(odd);
    }

    public Optional<Radix2Component> getEven() {
        return Optional.ofNullable(even);
    }

    public int getButterflyParameter_N() {
        return butterflyParameter_N;
    }

    public Object[] getButterflyParameter_Twiddle() {
        return butterflyParameter_Twiddle;
    }

    public String getPathToOperation() {
        return pathToOperation;
    }
}
