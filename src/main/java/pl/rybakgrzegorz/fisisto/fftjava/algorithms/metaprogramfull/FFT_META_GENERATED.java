package pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull;

import pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.generated_bitrev.*;

public class FFT_META_GENERATED {

    public static FFT_META_GENERATEDI createExecutorSupplier(int wSize) {
        switch (wSize) {
            case 8:
                return new FFT_META_GENERATED_8();
            case 16:
                return new FFT_META_GENERATED_16();
            case 32:
                return new FFT_META_GENERATED_32();
            case 64:
                return new FFT_META_GENERATED_64();
            case 128:
                return new FFT_META_GENERATED_128();
            case 256:
                return new FFT_META_GENERATED_256();
            case 512:
                return new FFT_META_GENERATED_512();
            case 1024:
                return new FFT_META_GENERATED_1024();
            case 2048:
                return new FFT_META_GENERATED_2048();
            default:
                throw new IllegalArgumentException("Cannot create executor for size: " + wSize);
        }
    }

}
