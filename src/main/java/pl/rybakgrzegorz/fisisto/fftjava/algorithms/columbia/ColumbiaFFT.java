package pl.rybakgrzegorz.fisisto.fftjava.algorithms.columbia;

import java.util.Arrays;
import java.util.List;

/**
 * Columbia FFT is sourced from Columbia University web page.
 * Some modification done:
 * 1. Extracted twiddle factors out of fft method.
 * 2. Compute PSD with non-parametrized Schuster estimator.
 *
 * @see https://www.ee.columbia.edu/~ronw/code/MEAPsoft/doc/html/STFT_8java-source.html
 */
public class ColumbiaFFT {

    /*
00002  *  Copyright 2006-2007 Columbia University.
00003  *
00004  *  This file is part of MEAPsoft.
00005  *
00006  *  MEAPsoft is free software; you can redistribute it and/or modify
00007  *  it under the terms of the GNU General Public License version 2 as
00008  *  published by the Free Software Foundation.
00009  *
00010  *  MEAPsoft is distributed in the hope that it will be useful, but
00011  *  WITHOUT ANY WARRANTY; without even the implied warranty of
00012  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
00013  *  General Public License for more details.
00014  *
00015  *  You should have received a copy of the GNU General Public License
00016  *  along with MEAPsoft; if not, write to the Free Software
00017  *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
00018  *  02110-1301 USA
00019  *
00020  *  See the file "COPYING" for the text of the license.
00021  */

    private int maxInputLength = 4096;

    private List<Integer> lengths = Arrays.asList(new Integer[]{4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096});
    private double[][] cos = new double[lengths.size()][maxInputLength / 2];
    private double[][] sin = new double[lengths.size()][maxInputLength / 2];

    {
        for (int lengthsIndex = 0; lengthsIndex < lengths.size(); lengthsIndex++) {
            int lengthToProcess = lengths.get(lengthsIndex);
            for (int i = 0; i < lengthToProcess / 2; i++) {
                cos[lengthsIndex][i] = Math.cos(-2 * Math.PI * i / lengthToProcess);
                sin[lengthsIndex][i] = Math.sin(-2 * Math.PI * i / lengthToProcess);
            }
        }

    }

    public static Object[] bitReverse(final Object[] in) {
        int j = 0;
        int n2 = in.length / 2;
        for (int i = 1; i <= in.length - 1; i++) {
            int n1 = n2;
            while (j >= n1) {
                j = j - n1;
                n1 = n1 / 2;
            }
            j = j + n1;
            if (i < j) {
                Object t1 = in[i];
                in[i] = in[j];
                in[j] = t1;
            }
        }
        return in;
    }

    public static double[] bitReverse(final double[] in) {
        int j = 0;
        int n2 = in.length / 2;
        for (int i = 1; i <= in.length - 1; i++) {
            int n1 = n2;
            while (j >= n1) {
                j = j - n1;
                n1 = n1 / 2;
            }
            j = j + n1;
            if (i < j) {
                double t1 = in[i];
                in[i] = in[j];
                in[j] = t1;
            }
        }
        return in;
    }

    /*
    * Compute FFT and its PSD with Schuster estimator.
    * */
    public double[] fftPSD(final double[] in) {
        int n = in.length;
        int m = (int) (Math.log(n) / Math.log(2));

        if (n != (1 << m)) {
            throw new RuntimeException("Input length is not a power of 2");
        }

        double[] cos2 = cos[lengths.indexOf(in.length)];
        double[] sin2 = sin[lengths.indexOf(in.length)];

        bitReverse(in);

        double[] iny = new double[in.length];

        int n1 = 0;
        int n2 = 1;

        for (int i = 0; i < m; i++) {
            n1 = n2;
            n2 = n2 + n2;
            int a = 0;
            for (int j = 0; j < n1; j++) {
                double c = cos2[a];
                double s = sin2[a];
                a += 1 << (m - i - 1);
                for (int k = j; k < n; k = k + n2) {
                    double t1 = c * in[k + n1] - s * iny[k + n1];
                    double t2 = s * in[k + n1] + c * iny[k + n1];
                    in[k + n1] = in[k] - t1;
                    iny[k + n1] = iny[k] - t2;
                    in[k] = in[k] + t1;
                    iny[k] = iny[k] + t2;
                }
            }

        }

        double[] out = new double[n];
        for (int i = 0; i < n; i++) {
            out[i] = Math.sqrt((in[i] * in[i]) + (iny[i] * iny[i])) / n * 2;
        }

        return out;
    }


    public double[] fft(double[] x) {
        double[] cos2 = cos[lengths.indexOf(x.length)];
        double[] sin2 = sin[lengths.indexOf(x.length)];

        int i, j, k, n1, n2, a;
        double c, s, t1, t2;
        int n = x.length;
        int m = (int) (Math.log(n) / Math.log(2));

        double[] y = new double[n];
        bitReverse(x);

        n2 = 1;

        for (i = 0; i < m; i++) {
            n1 = n2;
            n2 = n2 + n2;
            a = 0;

            for (j = 0; j < n1; j++) {
                c = cos2[a];
                s = sin2[a];
                a += 1 << (m - i - 1);

                for (k = j; k < n; k = k + n2) {
                    t1 = c * x[k + n1] - s * y[k + n1];
                    t2 = s * x[k + n1] + c * y[k + n1];
                    x[k + n1] = x[k] - t1;
                    y[k + n1] = y[k] - t2;
                    x[k] = x[k] + t1;
                    y[k] = y[k] + t2;
                }
            }
        }
        double[] out = new double[2 * n];
        for (int in = 0; in < n; in++) {
            out[in * 2] = x[in];
            out[in * 2 + 1] = y[in];
        }
        return out;
    }

}