package pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.butterflyoperations;

public class Operation {

    public enum Type{
        MERGE, COPY_TO_TABLE, COPY_FROM_TABLE, BIT_REVERSE, NEXT_STAGE, MERGE_AND_BIT_REVERSE
    }

    public Type type;

    public int stage;
    public int thread;
    public int op_1;
    public int op_2;
    public int op_3;
    public int op_4;
    public int op_5;
    public int op_6;

    public String operationPath;

    @Override
    public String toString() {
        return "Operation{" +
                "type=" + type +
                ", stage=" + stage +
                ", thread=" + thread +
                ", op_1=" + op_1 +
                ", op_2=" + op_2 +
                ", op_3=" + op_3 +
                ", op_4=" + op_4 +
                ", op_5=" + op_5 +
                ", op_6=" + op_6 +
                ", operationPath='" + operationPath + '\'' +
                '}';
    }
}
