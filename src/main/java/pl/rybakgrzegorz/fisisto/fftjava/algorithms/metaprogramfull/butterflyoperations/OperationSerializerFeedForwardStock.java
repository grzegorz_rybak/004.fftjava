package pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.butterflyoperations;

import pl.rybakgrzegorz.fisisto.fftjava.algorithms.columbia.ColumbiaFFT;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class OperationSerializerFeedForwardStock {

    public Map<Integer, Object> serializer(final int windowSize, OperationExtender operationExender) {

        final List<Operation> FIRST = new ArrayList();
        final List<Operation> SECOND = new ArrayList();

        List<Operation> FIRST_AFTER_CLEAR_TWOS = new ArrayList();
        List<Operation> FIRST_AFTER_CLEAR_REST = new ArrayList();
        List<Operation> SECOND_AFTER_CLEAR_TWOS = new ArrayList();
        List<Operation> SECOND_AFTER_CLEAR_REST = new ArrayList();

        List<Operation> COPY_FROM_TABLE = new ArrayList();

        List<Operation> BIT_REVERSE = prepareBitReverseOperations(windowSize);

        operationExender.firstExecution.forEach((k, v) -> {
            FIRST.addAll(v.stream().filter(x -> x.stage > 0).collect(Collectors.toList()));
        });

        FIRST.stream().filter(x -> x.type == Operation.Type.MERGE).forEach(op -> {
            int tableSize = operationExender.getTransitionTableSize() - 2;
            op.op_4 = setCopyToTransitionTableIndex(op.stage,tableSize, op.op_1, FIRST);
            op.op_5 = setCopyToTransitionTableIndex(op.stage,tableSize, op.op_2, FIRST);
        });

        FIRST_AFTER_CLEAR_TWOS = FIRST.stream().filter(x -> x.type != Operation.Type.COPY_TO_TABLE).filter(x->x.stage==1).collect(Collectors.toList());
        FIRST_AFTER_CLEAR_REST = FIRST.stream().filter(x -> x.type != Operation.Type.COPY_TO_TABLE).filter(x->x.stage>1).collect(Collectors.toList());

        operationExender.secondExecution.forEach((k, v) -> {
            SECOND.addAll(v.stream().filter(x -> x.stage > 0).collect(Collectors.toList()));
        });

        COPY_FROM_TABLE = SECOND.stream().filter(x -> x.type == Operation.Type.COPY_FROM_TABLE).collect(Collectors.toList());

        SECOND.stream().filter(x -> x.type == Operation.Type.MERGE).forEach(op -> {
            int tableSize = operationExender.getTransitionTableSize() - 2;
            op.op_4 = setCopyToTransitionTableIndex(op.stage,tableSize, op.op_1, SECOND);
            op.op_5 = setCopyToTransitionTableIndex(op.stage,tableSize, op.op_2, SECOND);
        });

        SECOND_AFTER_CLEAR_TWOS = SECOND.stream().filter(x -> ((x.type != Operation.Type.COPY_TO_TABLE) && (x.type != Operation.Type.COPY_FROM_TABLE))).filter(x->x.stage==1).collect(Collectors.toList());
        SECOND_AFTER_CLEAR_REST = SECOND.stream().filter(x -> ((x.type != Operation.Type.COPY_TO_TABLE) && (x.type != Operation.Type.COPY_FROM_TABLE))).filter(x->x.stage>1).collect(Collectors.toList());

        int stages =  SECOND_AFTER_CLEAR_REST.stream().map(x->x.stage).max(Integer::compare).get();
        Map<Integer/*Thread*/, Object/*intArray*/> out = new TreeMap<>();
        out.put(0, convertToTable(FIRST_AFTER_CLEAR_TWOS, stages));
        out.put(1, convertToTable(FIRST_AFTER_CLEAR_REST, stages));

        out.put(2, convertToTable(COPY_FROM_TABLE, stages));
        out.put(3, convertToTable(SECOND_AFTER_CLEAR_TWOS, stages));
        out.put(4, convertToTable(SECOND_AFTER_CLEAR_REST, stages));

        out.put(5, convertToTable(BIT_REVERSE, stages));
        return out;
    }

    private int[] convertToTable(List<Operation> list, int stages) {
        final int opCount = 6;
        final int[] table = new int[list.size() * opCount];
        for (int i = 0; i < list.size(); i++) {
            Operation operation = list.get(i);
            switch (operation.type) {
                case BIT_REVERSE:
                case MERGE:
                    table[i * opCount] = operation.op_1 * 2;
                    table[i * opCount + 1] = operation.op_2 * 2;
                    table[i * opCount + 2] = operation.op_3;
                    table[i * opCount + 3] = operation.op_4; // copy to transition table from L element
                    table[i * opCount + 4] = operation.op_5; // copy to transition table from R element
                    table[i * opCount + 5] = stages-operation.stage;
                    break;
                case COPY_TO_TABLE:
                    table[i * opCount] = -1;
                    table[i * opCount + 1] = operation.op_1 * 2;
                    table[i * opCount + 2] = operation.op_2;  // transition table index
                    table[i * opCount + 5] = stages-operation.stage;
                    break;
                case COPY_FROM_TABLE:
                    table[i * opCount] = -2;
                    table[i * opCount + 1] = operation.op_1 * 2;
                    table[i * opCount + 2] = operation.op_2; // transition table index
                    table[i * opCount + 5] = stages-operation.stage;
                    break;
            }

        }
        return table;
    }

    private int setCopyToTransitionTableIndex(int stage, int transitionTableLastElement, int currentTableIndex, List<Operation> list) {
        for (Operation op : list) {
            if (op.stage == stage && Operation.Type.COPY_TO_TABLE.equals(op.type) && ((op.op_1 == currentTableIndex))) {
                return op.op_2;
            }
        }
        return -1;//transitionTableLastElement;
    }

    private List<Operation> prepareBitReverseOperations(final int windowSize) {
        final List<Operation> operationsForReverse = new ArrayList<>();

        Object[] tab = ColumbiaFFT.bitReverse(IntStream.range(0, windowSize).boxed().toArray());
        for (int i = 0; i < tab.length; i++) {
            // if (i < ((int) tab[i])) {
            //System.out.println(String.format("A %s to %s", i, (int) tab[i]));
            Operation operation = new Operation();
            operation.type = Operation.Type.BIT_REVERSE;
            operation.op_1 = i;
            operation.op_2 = (int) tab[i];
            operation.op_3 = 7777;
            operationsForReverse.add(operation);
            //   }
        }
        return operationsForReverse;
    }

}
