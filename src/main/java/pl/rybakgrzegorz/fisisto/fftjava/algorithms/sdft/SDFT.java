package pl.rybakgrzegorz.fisisto.fftjava.algorithms.sdft;

import pl.rybakgrzegorz.fisisto.fftjava.algorithms.recursive.Recursive;

/**
 * Created by TERMINAL_T1_USER on 2016-06-13.
 */
public class SDFT {

    private boolean first = true;
    private double[][] fftCacheSDFT = new double[1][1];
    private double firstElementFromPreviousWindow;

    private Recursive recursive = new Recursive();

    public double[][] compute(double[] input) {
        if (first) {
            fftCacheSDFT = recursive.compute(input, 0);
            first = false;
        } else {
            int windowSize = fftCacheSDFT[0].length;
            for (int i = 0; i < windowSize; i++) {
                double real = fftCacheSDFT[0][i];
                double imag = fftCacheSDFT[1][i];
                double[] complex = new double[]{real, imag};
                double diff = input[input.length - 1] - firstElementFromPreviousWindow;
                double[] add = addComplex(complex, new double[]{diff, Double.valueOf(0)});
                double W = 2.0 * Math.PI * (Double.valueOf(i) / Double.valueOf(windowSize));
                double[] multi = multiplyComplex(new double[]{Math.cos(W), Math.sin(W)}, add);
                fftCacheSDFT[0][i] = multi[0];
                fftCacheSDFT[1][i] = multi[1];
            }
        }
        firstElementFromPreviousWindow = input[0];
        return fftCacheSDFT;
    }

    private double[] addComplex(double[] f, double[] sec) {
        return new double[]{f[0] + sec[0], f[1] + sec[1]};
    }

    private double[] multiplyComplex(double[] f, double[] sec) {
        return new double[]{(f[0] * sec[0] - f[1] * sec[1]), (f[1] * sec[0] + f[0] * sec[1])};
    }

    public void cleanCache() {
        first = true;
        fftCacheSDFT = new double[1][1];
        firstElementFromPreviousWindow = 0;
    }
}
