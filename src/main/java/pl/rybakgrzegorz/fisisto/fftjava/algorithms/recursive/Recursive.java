package pl.rybakgrzegorz.fisisto.fftjava.algorithms.recursive;

public class Recursive {
    public double[][] compute(double[] input, int dep) {
        if (input.length == 1) {
            return new double[][]{input, new double[]{0}};
        } else {
            double[] TL = new double[input.length / 2];
            double[] TR = new double[input.length / 2];

            for (int i = 0; i < input.length - 1; i += 2) {
                TL[i / 2] = input[i];
                TR[i / 2] = input[i + 1];
            }
            double[][] retL = compute(TL, dep + 1);
            double[][] retR = compute(TR, dep + 1);

            double[][] returnTable = new double[2][input.length];

            int reverseOrder = 1;
            for (int i = 0; i < input.length / 2; i++) {

                double angle = ((double) -2) * Math.PI * i / input.length;
                double[] factor = prepareFactor(angle);
                double[] factor2 = new double[]{factor[0] * (-1), factor[1] * (-1)};

                double[] complexL = new double[]{retL[0][i], retL[1][i]};
                double[] complexR = new double[]{retR[0][i], retR[1][i]};

                double[] o = addComplex(complexL, multiplyComplex(factor, complexR));
                //returnTable[0][i] = Math.sqrt(Math.pow(o[0], 2) + Math.pow(o[1], 2)) / (input.length / 2);
                returnTable[0][i] = o[0];
                returnTable[1][i] = o[1] * reverseOrder;

                double[] o2 = addComplex(complexL, multiplyComplex(factor2, complexR));
                //returnTable[0][input.length / 2 + i] = Math.sqrt(Math.pow(o2[0], 2) + Math.pow(o2[1], 2)) / (input.length / 2);
                returnTable[0][input.length / 2 + i] = o2[0];
                returnTable[1][input.length / 2 + i] = o2[1] * reverseOrder;
            }
            return returnTable;
        }
    }

    private double[] prepareFactor(double angle) {
        return new double[]{Math.cos(angle), Math.sin(angle)};
    }

    private double[] addComplex(double[] f, double[] sec) {
        return new double[]{f[0] + sec[0], f[1] + sec[1]};
    }

    private double[] multiplyComplex(double[] f, double[] sec) {
        return new double[]{(f[0] * sec[0] - f[1] * sec[1]), (f[1] * sec[0] + f[0] * sec[1])};
    }

}