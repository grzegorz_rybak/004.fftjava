package pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.butterflyoperations;


import com.google.common.collect.Multimap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.butterflyoperations.transitions.TransitionsManager;


import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class OperationExtender {

    private static final Logger LOGGER = LoggerFactory.getLogger(OperationExtender.class);

    final Map<Integer, List<Operation>> firstExecution = new TreeMap<>();
    final Map<Integer, List<Operation>> secondExecution = new TreeMap<>();

    private int transitionTableSize;

    public Map<Integer, List<Operation>> extend(Map<Integer, List<Operation>> operations, TransitionsManager transitionsManager) {
        final Multimap<Integer, TransitionsManager.Pair> transitions = transitionsManager.getMap();

        transitionTableSize = codeTransitionTable(transitions);

        operations.forEach((stage, operationsInStage) -> {
            toFirstExecAddOperations(stage, operationsInStage);
            toFirstExecAddCopyToTableTransitions(stage, transitions.get(stage));

            toSecondExecAddCopyFromTableTransitions(stage, transitions.get(stage));
            toSecondExecAddReducedOperations(stage, operationsInStage, transitions);
            toSecondExecAddCopyToTableTransitions(stage, transitions.get(stage));
        });

        //logResult();

        return firstExecution;// + secondeExcution should be !!
    }

    public int maxStageNr(){
        return this.secondExecution.keySet().stream().mapToInt(x->x).max().getAsInt();
    }

    private int codeTransitionTable(Multimap<Integer, TransitionsManager.Pair> transitions) {
        AtomicInteger transitionTableIndex = new AtomicInteger(0);
        transitions.forEach((stage, pair) -> {
            pair.transitionTableStartIndex = transitionTableIndex.get();
            transitionTableIndex.addAndGet(pair.first.getFftData().size() * 2);
        });
        return transitionTableIndex.get();
    }

    private void toSecondExecAddReducedOperations(Integer stage, List<Operation> operationsInStage, Multimap<Integer, TransitionsManager.Pair> transitions) {
        secondExecution.putIfAbsent(stage, new ArrayList<>());
        operationsInStage.stream().forEach(operation -> {
            if (operationIsNotInTransition(stage, transitions, operation)) {
                secondExecution.get(stage).add(operation);
            }
        });
    }

    private boolean operationIsNotInTransition(Integer processingState, Multimap<Integer, TransitionsManager.Pair> transitions, Operation operation) {
        AtomicLong sum = new AtomicLong(0);
        transitions.forEach((stage, pair) -> {
            if (stage >= processingState) {
                sum.addAndGet(pair.second.getFftData().stream().map(i -> i.getKey()).filter(in -> (in == operation.op_1) || (in == operation.op_2)).count());
            }
        });
        return sum.get() == 0;
    }

    private void toSecondExecAddCopyToTableTransitions(Integer stage, Collection<TransitionsManager.Pair> pairs) {
        secondExecution.putIfAbsent(stage, new ArrayList<>());
        pairs.stream().forEach(pair -> {
            for (int i = 0; i < pair.first.getFftData().size(); i++) {
                int indexFrom = pair.first.getFftData().get(i).getKey();
                final Operation operation = new Operation();
                operation.stage = stage;
                operation.type = Operation.Type.COPY_TO_TABLE;
                operation.op_1 = indexFrom;
                operation.op_2 = pair.transitionTableStartIndex + (i * 2);
                secondExecution.get(stage).add(operation);
            }
        });
    }

    private void toSecondExecAddCopyFromTableTransitions(Integer stage, Collection<TransitionsManager.Pair> pairs) {
        secondExecution.putIfAbsent(stage, new ArrayList<>());
        pairs.stream().forEach(pair -> {
            for (int i = 0; i < pair.second.getFftData().size(); i++) {
                int indexFrom = pair.second.getFftData().get(i).getKey();
                final Operation operation = new Operation();
                operation.stage = stage;
                operation.type = Operation.Type.COPY_FROM_TABLE;
                operation.op_1 = indexFrom;
                operation.op_2 = pair.transitionTableStartIndex + (i * 2);
                secondExecution.get(stage).add(operation);
            }
        });
    }

    private void toFirstExecAddCopyToTableTransitions(Integer stage, Collection<TransitionsManager.Pair> pairs) {
        firstExecution.putIfAbsent(stage, new ArrayList<>());
        pairs.stream().forEach(pair -> {
            for (int i = 0; i < pair.first.getFftData().size(); i++) {
                int indexFrom = pair.first.getFftData().get(i).getKey();
                final Operation operation = new Operation();
                operation.stage = stage;
                operation.type = Operation.Type.COPY_TO_TABLE;
                operation.op_1 = indexFrom;
                operation.op_2 = pair.transitionTableStartIndex + (i * 2);
                firstExecution.get(stage).add(operation);
            }
        });
    }

    private void toFirstExecAddOperations(Integer stage, List<Operation> operationsInStage) {
        firstExecution.putIfAbsent(stage, new ArrayList<>());
        firstExecution.get(stage).addAll(operationsInStage);
    }

    public void logResult() {
        LOGGER.debug("First Execution");
        firstExecution.entrySet().stream().forEach((entry) -> {
            entry.getValue().stream().forEach(operation -> LOGGER.debug("\tStage: {} - {}", entry.getKey(), operation));
        });
        LOGGER.debug("Second Execution");
        secondExecution.entrySet().stream().forEach((entry) -> {
            entry.getValue().stream().forEach(operation -> LOGGER.debug("\tStage: {} - {}", entry.getKey(), operation));
        });
    }

    /**
     * Add value two to extends table for dummy copy.
     * */
    public int getTransitionTableSize() {
        return transitionTableSize +2;
    }
}
