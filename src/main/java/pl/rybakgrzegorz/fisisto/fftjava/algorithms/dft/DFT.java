package pl.rybakgrzegorz.fisisto.fftjava.algorithms.dft;

/**
 * Basic implementation of Fourier Transform.
 * Discrete Fourier Transform - brute force approach.
 */
public class DFT {

    /**
     * Compute Fourier Transform based on brute force DFT algorithm.
     *
     * @param in - vector of real values.
     */
    public double[] compute(double[] in) {
        double[] out = new double[2*in.length];
        double[] or = new double[in.length];
        double[] oi = new double[in.length];
        double[] fakeTable = new double[in.length];
        compute(in, fakeTable, or, oi);
        for (int i = 0; i < or.length; i++) {
            out[i*2] = or[i];
            out[i*2+1] = oi[i];
        }
        return out;
    }

    public void compute(double[] inreal, double[] inimag, double[] outreal, double[] outimag) {
        int n = inreal.length;
        for (double k = 0; k < n; k++) {  // For each output element
            double sumreal = 0;
            double sumimag = 0;
            for (double t = 0; t < n; t++) {  // For each input element
                double angle = (2 * Math.PI * t * k / n);
                sumreal += inreal[(int) t] * Math.cos(angle) + inimag[(int) t] * Math.sin(angle);
                sumimag -= inreal[(int) t] * Math.sin(angle) + inimag[(int) t] * Math.cos(angle);
            }
            outreal[(int) k] = sumreal;
            outimag[(int) k] = sumimag;
        }
    }

}
