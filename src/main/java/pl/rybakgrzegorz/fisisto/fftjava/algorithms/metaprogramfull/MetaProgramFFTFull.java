package pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull;

import com.squareup.javapoet.*;
import org.slf4j.LoggerFactory;
import pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.butterflyoperations.OperationExtender;
import pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.butterflyoperations.OperationFactory;
import pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.butterflyoperations.OperationSerializerFeedForwardStock;

import javax.lang.model.element.Modifier;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.IntStream;

public class MetaProgramFFTFullStockham {

    public static final String FCS_DSP_SRC_MAIN_JAVA = "./src/main/java";

    public static final int START_INCLUSIVE_EQ_8 = 3;
    public static final int END_EXCLUSIVE_EQ_1024 = 11;
    public static final int END_EXCLUSIVE_EQ_2048 = 12;

    private static final boolean DONT_CHANGE_METHOD_REF = false;
    private static final boolean CHANGE_METHOD_REF_TO_NEXT_ITERATIONS = true;

    private static double[] FACTOR2 = TwiddleFactors.FACTOR2;
    private static double[] FACTOR3 = TwiddleFactors.FACTOR3;

    private class MethodBuilderWithLineSplitter {
        public static final int APROX_LINE_COUNT = 700;
        private long linesCounter = 0;
        private List<CodeBlock.Builder> methods = new ArrayList<>();
        private CodeBlock.Builder current_BUILDER = CodeBlock.builder();

        public void startBlock() {
            if ((methods.size() * APROX_LINE_COUNT) <= linesCounter) {
                current_BUILDER = CodeBlock.builder();
                methods.add(current_BUILDER);
            }
        }

        public void addStatement(final String format, final Object... args) {
            current_BUILDER.addStatement(format, args);
            linesCounter++;
        }

        public List<CodeBlock.Builder> getMethods() {
            return methods;
        }
    }

    public static void main(String[] args) throws IOException {
        final ParameterizedTypeName consumerWithGeneric = ParameterizedTypeName
                .get(ClassName.get(Consumer.class), WildcardTypeName.supertypeOf(double[].class));
        final FieldSpec executor = FieldSpec.builder(consumerWithGeneric, "executor")
                .addModifiers(Modifier.PUBLIC)
                .build();
        final TypeSpec abstractClass = TypeSpec
                .classBuilder("FFT_META_GENERATEDI")
                .addModifiers(Modifier.PUBLIC)
                .addModifiers(Modifier.ABSTRACT)
                .addField(executor)
                .build();

        final JavaFile javaFile = JavaFile
                .builder(MetaProgramFFTFullStockham.class.getPackage().getName()+".generated_stock", abstractClass)
                .indent("    ")
                .build();
        final Path path = Paths.get(FCS_DSP_SRC_MAIN_JAVA);
        javaFile.writeTo(path);

        final MetaProgramFFTFullStockham poetMainCopy = new MetaProgramFFTFullStockham();
        IntStream.range(START_INCLUSIVE_EQ_8, END_EXCLUSIVE_EQ_2048).forEach(x -> {
            try {
                poetMainCopy.generateForWindowSize((int) Math.pow(2, x), abstractClass);
            } catch (IOException e) {
                LoggerFactory.getLogger(MetaProgramFFTFullStockham.class).error("Cannot generate code", e);
            }
        });
    }

    public void generateForWindowSize(int windowSize, TypeSpec abstractClass) throws IOException {
        final OperationSerializerFeedForwardStock serializer = new OperationSerializerFeedForwardStock();
        final OperationExtender operationExtender = OperationFactory.prepareDataForFFT(windowSize, 1);
        Map<Integer, Object> code = serializer.serializer(windowSize, operationExtender);

        final MetaProgramFFTFullStockham poetMain = new MetaProgramFFTFullStockham();
        poetMain.run(code, operationExtender.getTransitionTableSize(), windowSize, abstractClass);
    }


    private void run(final Map<Integer, Object> code, int transitionTableSize, int windowSize, TypeSpec abstractClass) throws IOException {
        final String className = "FFT_META_GENERATED_" + windowSize;
        final FieldSpec transitionTable = FieldSpec.builder(double[].class, "transitionTable")
                .addModifiers(Modifier.PRIVATE)
                .initializer(CodeBlock.builder().add("new double[" + transitionTableSize + "]").build()).build();

        final FieldSpec stockhamTable = FieldSpec.builder(double[].class, "stockhamTable")
                .addModifiers(Modifier.PRIVATE)
                .initializer(CodeBlock.builder().add("new double[" + windowSize * 2 + "]").build()).build();

        final TypeSpec className_FFT_META_GENERATED = TypeSpec
                .classBuilder(className)
                .superclass(ClassName.get("", abstractClass.name))
                .addModifiers(Modifier.PUBLIC)
                .addInitializerBlock(CodeBlock.builder().addStatement("executor = this::fft").build())
                .addField(transitionTable)
                .addField(stockhamTable)
                .addMethods(createMethodCompute(className, (int[]) code.get(0), (int[]) code.get(1), (int[]) code.get(5)))
                .addMethods(createMethodCompute_next_iterations(className, (int[]) code.get(2), (int[]) code.get(3), (int[]) code.get(4), (int[]) code.get(5)))
                .build();

        final JavaFile javaFile = JavaFile
                .builder(this.getClass().getPackage().getName()+".generated_stock", className_FFT_META_GENERATED)
                .indent("    ")
                .build();
        final Path path = Paths.get(FCS_DSP_SRC_MAIN_JAVA);
        javaFile.writeTo(path);
    }

    private List<MethodSpec> createMethodCompute(String className, final int[] operation_for_two_element_fft,
                                                 final int[] operation_for_rest_stages,
                                                 final int[] operation_for_bit_reverse) {
        final MethodBuilderWithLineSplitter method_computeImpl_BUILDER = new MethodBuilderWithLineSplitter();

        createMethodComputeGeneric(operation_for_two_element_fft, operation_for_rest_stages, operation_for_bit_reverse, method_computeImpl_BUILDER);
        return getMethodSpec(className, method_computeImpl_BUILDER, "fft", CHANGE_METHOD_REF_TO_NEXT_ITERATIONS);
    }

    private List<MethodSpec> createMethodCompute_next_iterations(String className, final int[] operation_for_copy_from_table,
                                                                 final int[] operation_for_two_element_fft,
                                                                 final int[] operation_for_rest_stages,
                                                                 final int[] operation_for_bit_reverse) {
        final MethodBuilderWithLineSplitter method_computeImpl_BUILDER = new MethodBuilderWithLineSplitter();

        int[] operations_copyFrom = operation_for_copy_from_table;
        for (int i = 0; i < operations_copyFrom.length; i += 6) {
            String tableTarget = operations_copyFrom[i + 5] <= 1 ? "stockhamTable" : "current";
            method_computeImpl_BUILDER.startBlock();
            method_computeImpl_BUILDER.addStatement(tableTarget + "[$L] = transitionTable[$L]", operations_copyFrom[i + 1], operations_copyFrom[i + 2]);
            method_computeImpl_BUILDER.addStatement(tableTarget + "[$L] = transitionTable[$L]", operations_copyFrom[i + 1] + 1, operations_copyFrom[i + 2] + 1);
        }

        createMethodComputeGeneric(operation_for_two_element_fft, operation_for_rest_stages, operation_for_bit_reverse, method_computeImpl_BUILDER);
        return getMethodSpec(className, method_computeImpl_BUILDER, "fft_next_iterations", DONT_CHANGE_METHOD_REF);
    }

    private MethodBuilderWithLineSplitter createMethodComputeGeneric(final int[] operation_for_two_element_fft,
                                                                     final int[] operation_for_rest_stages,
                                                                     final int[] operation_for_bit_reverse,
                                                                     final MethodBuilderWithLineSplitter method_computeImpl_BUILDER) {

        // process twos
        int[] operations_twos = operation_for_two_element_fft;
        for (int i = 0; i < operations_twos.length; i += 6) {
            method_computeImpl_BUILDER.startBlock();
            method_computeImpl_BUILDER.addStatement("tempR = current[$L]", operations_twos[i + 1]);
            if (operations_twos[i + 3] >= 0) {
                method_computeImpl_BUILDER.addStatement("transitionTable[$L] = current[$L] = current[$L] - tempR", operations_twos[i + 4], operations_twos[i + 1], operations_twos[i]);
                method_computeImpl_BUILDER.addStatement("transitionTable[$L] = current[$L] = current[$L] + tempR", operations_twos[i + 3], operations_twos[i], operations_twos[i]);
            } else {
                method_computeImpl_BUILDER.addStatement("current[$L] = current[$L] - tempR", operations_twos[i + 1], operations_twos[i]);
                method_computeImpl_BUILDER.addStatement("current[$L] = current[$L] + tempR", operations_twos[i], operations_twos[i]);
            }
        }

        // process rest
        int[] operations_rest = operation_for_rest_stages;
        for (int i = 0; i < operations_rest.length; i += 6) {
            String tableSource = operations_rest[i + 5]==0 ? "stockhamTable":"current";
            String tableTarget = operations_rest[i + 5]==1 ? "stockhamTable":"current";

            method_computeImpl_BUILDER.startBlock();
            method_computeImpl_BUILDER.addStatement("tempR = " + tableSource + "[$L]", operations_rest[i + 1]);
            method_computeImpl_BUILDER.addStatement("tempI = " + tableSource + "[$L]", operations_rest[i + 1] + 1);

            optimizeTwiddleFactorMultiplication(method_computeImpl_BUILDER, "FR", "tempR", FACTOR2[operations_rest[i + 2]]);
            optimizeTwiddleFactorMultiplication(method_computeImpl_BUILDER, "FI", "tempR", FACTOR3[operations_rest[i + 2]]);
            optimizeTwiddleFactorMultiplication(method_computeImpl_BUILDER, "FR2", "tempI", FACTOR3[operations_rest[i + 2]]);
            optimizeTwiddleFactorMultiplication(method_computeImpl_BUILDER, "FI2", "tempI", FACTOR2[operations_rest[i + 2]]);
            if (FACTOR3[operations_rest[i + 2]] != 0) {
                method_computeImpl_BUILDER.addStatement("FR -= FR2");
            }
            if (FACTOR2[operations_rest[i + 2]] != 0) {
                method_computeImpl_BUILDER.addStatement("FI += FI2");
            }

            if (operations_rest[i + 3] >= 0) {
                method_computeImpl_BUILDER.addStatement("transitionTable[$L] = "+tableTarget+"[$L] = "+tableSource+"[$L] - FR", operations_rest[i + 4],bitReversed(operation_for_bit_reverse, operations_rest[i + 1],operations_rest[i + 5]), operations_rest[i]);
                method_computeImpl_BUILDER.addStatement("transitionTable[$L] = "+tableTarget+"[$L] = "+tableSource+"[$L] - FI", operations_rest[i + 4] + 1,bitReversed(operation_for_bit_reverse,  operations_rest[i + 1] + 1,operations_rest[i + 5]), operations_rest[i] + 1);
                method_computeImpl_BUILDER.addStatement("transitionTable[$L] = "+tableTarget+"[$L] = "+tableSource+"[$L] + FR", operations_rest[i + 3], bitReversed(operation_for_bit_reverse, operations_rest[i],operations_rest[i + 5]), operations_rest[i]);
                method_computeImpl_BUILDER.addStatement("transitionTable[$L] = "+tableTarget+"[$L] = "+tableSource+"[$L] + FI", operations_rest[i + 3] + 1, bitReversed(operation_for_bit_reverse, operations_rest[i] + 1,operations_rest[i + 5]), operations_rest[i] + 1);
            } else {
                method_computeImpl_BUILDER.addStatement(tableTarget+"[$L] = "+tableSource+"[$L] - FR", bitReversed(operation_for_bit_reverse, operations_rest[i + 1],operations_rest[i + 5]), operations_rest[i]);
                method_computeImpl_BUILDER.addStatement(tableTarget+"[$L] = "+tableSource+"[$L] - FI", bitReversed(operation_for_bit_reverse, operations_rest[i + 1] + 1,operations_rest[i + 5]), operations_rest[i] + 1);
                method_computeImpl_BUILDER.addStatement(tableTarget+"[$L] = "+tableSource+"[$L] + FR", bitReversed(operation_for_bit_reverse, operations_rest[i],operations_rest[i + 5]), operations_rest[i]);
                method_computeImpl_BUILDER.addStatement(tableTarget+"[$L] = "+tableSource+"[$L] + FI", bitReversed(operation_for_bit_reverse, operations_rest[i] + 1,operations_rest[i + 5]), operations_rest[i] + 1);
            }
        }

        return method_computeImpl_BUILDER;
    }

    private int bitReversed(int[] operation_for_bit_reverse, int address, int stage) {
        if (stage > 0) return address;
        return operation_for_bit_reverse[((address / 2) * 6) + 1] + (address % 2);
    }

    private void optimizeTwiddleFactorMultiplication(MethodBuilderWithLineSplitter method_computeImpl_BUILDER, String target, String source, double tw) {
        if (tw == 0) {
            method_computeImpl_BUILDER.addStatement(target + " = 0");
        } else if (tw == 1) {
            method_computeImpl_BUILDER.addStatement(target + " = " + source);
        } else if (tw == -1) {
            method_computeImpl_BUILDER.addStatement(target + " = -1 * " + source);
        } else {
            method_computeImpl_BUILDER.addStatement(target + " = $L * " + source, tw);
        }
    }

    private List<MethodSpec> getMethodSpec(String className, final MethodBuilderWithLineSplitter method_computeImpl_BUILDER, final String methodName, final boolean changeReferenceToMethod) {
        final List<MethodSpec> list = new ArrayList<>();
        final List<CodeBlock.Builder> method_computeImpl = method_computeImpl_BUILDER.getMethods();

        list.add(MethodSpec
                .methodBuilder(methodName)
                .addParameter(double[].class, "current")
                .addModifiers(Modifier.PRIVATE)
                .addCode(methodsInvocations(className, method_computeImpl, methodName, changeReferenceToMethod))
                .build());

        for (int i = 0; i < method_computeImpl.size(); i++) {
            final CodeBlock.Builder methodBuilder = method_computeImpl.get(i);
            list.add(MethodSpec
                    .methodBuilder(methodName + "_" + (i))
                    .addParameter(double[].class, "current")
                    .addModifiers(Modifier.PRIVATE)
                    .addModifiers(Modifier.STATIC)
                    .addParameter(double[].class, "transitionTable")
                    .addParameter(double[].class, "stockhamTable")
                    .addCode(prepareVariablesInTargetMethod())
                    .addCode(methodBuilder.build())
                    .build());
        }
        return list;
    }

    private CodeBlock methodsInvocations(final String className, final List<CodeBlock.Builder> method_computeImpl, final String methodName, final boolean changeReferenceToMethod) {
        final MethodBuilderWithLineSplitter method_computeImpl_BUILDER = new MethodBuilderWithLineSplitter();
        method_computeImpl_BUILDER.startBlock();
        for (int i = 0; i < method_computeImpl.size(); i++) {
            method_computeImpl_BUILDER.addStatement(className+".$L_$L(current,transitionTable, stockhamTable)", methodName, i);
        }
        if(changeReferenceToMethod) {
            method_computeImpl_BUILDER.addStatement("executor = this::fft_next_iterations");
        }
        return method_computeImpl_BUILDER.current_BUILDER.build();
    }

    private CodeBlock prepareVariablesInTargetMethod() {
        final MethodBuilderWithLineSplitter method_computeImpl_BUILDER = new MethodBuilderWithLineSplitter();
        method_computeImpl_BUILDER.startBlock();
        method_computeImpl_BUILDER.addStatement("double tempR = 0");
        method_computeImpl_BUILDER.addStatement("double tempI = 0");
        method_computeImpl_BUILDER.addStatement("double FI = 0");
        method_computeImpl_BUILDER.addStatement("double FR = 0");
        method_computeImpl_BUILDER.addStatement("double FI2 = 0");
        method_computeImpl_BUILDER.addStatement("double FR2 = 0");
        return method_computeImpl_BUILDER.current_BUILDER.build();
    }

}
