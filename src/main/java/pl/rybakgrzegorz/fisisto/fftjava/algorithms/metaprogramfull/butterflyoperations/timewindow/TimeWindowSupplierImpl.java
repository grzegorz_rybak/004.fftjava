package pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.butterflyoperations.timewindow;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TimeWindowSupplierImpl implements TimeWindowSupplier {

    private final int windowSize;

    private final int offset;

    public TimeWindowSupplierImpl(final int windowSize, int offset) {
        if (offset == 0) {
            this.windowSize = windowSize;
            this.offset = -windowSize;
        } else {
            this.windowSize = windowSize;
            this.offset = offset;
        }
    }

    @Override
    public List<Integer> supplyFirstTimeWindow() {
        return IntStream.range(0, windowSize).boxed().collect(Collectors.toList());
    }

    @Override
    public List<Integer> supplySecondTimeWindow() {
        return IntStream.range(offset, windowSize + offset).boxed().collect(Collectors.toList());
    }

}
