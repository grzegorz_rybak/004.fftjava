package pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull;

public class TwiddleFactors {

    public static int SIZE = 4096;
    public static int REAL_SIZE = SIZE + 1;

    public static double[] FACTOR2 = new double[REAL_SIZE*REAL_SIZE];
    public static double[] FACTOR3 = new double[REAL_SIZE*REAL_SIZE];

    static {
        for (int ss = 0; ss < REAL_SIZE; ss++) {
            for (int i = 0; i <= ss; i++) {
                float angle = (float) (((float) -2) * Math.PI * i / (ss));
                FACTOR2[REAL_SIZE * ss + i] = Math.cos(angle);
                FACTOR3[REAL_SIZE * ss + i] = Math.sin(angle);
            }
        }
    }

}
