package pl.rybakgrzegorz.fisisto.fftjava.algorithms.idft;

/**
 * Inverse Discrete Fourier Transform - IDFT.
 */
public class IDFT {

    /**
     * Compute inverse DFT.
     *
     * @param inreal real values
     * @param inimag imaginary values
     */
    public double[] idft(double[] inreal, double[] inimag) {
        int n = inreal.length;
        double out[] = new double[inreal.length];
        for (int k = 0; k < n; k++) {  // For each output element
            for (int t = 0; t < n; t++) {  // For each input element
                double angle = (2 * Math.PI * t * k / n);
                out[k] += (inreal[t] * Math.cos(angle) + inimag[t] * Math.sin(angle));
            }
            out[k] = out[k] / n;
        }
        return out;
    }

}
