package pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramcore;

import pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.TwiddleFactors;

import java.util.Map;

public class MetaProgramFFTCore {

    private static double[] FACTOR2 = TwiddleFactors.FACTOR2;
    private static double[] FACTOR3 = TwiddleFactors.FACTOR3;


    private final int[] operations_copyFrom_origin;
    private int[] operations_copyFrom;
    private int[] operations_twos;
    private int[] operations_rest;
    private int[] operations_second_twos;
    private int[] operations_second_rest;
    private int[] operations_bitReverse;

    private double[] transitionTable;


    public MetaProgramFFTCore(final Map<Integer, Object> code, int transitionTableSize) {
        operations_twos = (int[]) code.get(0);
        operations_rest = (int[]) code.get(1);
        operations_copyFrom_origin = (int[]) code.get(2);
        operations_second_twos = (int[]) code.get(3);
        operations_second_rest = (int[]) code.get(4);
        operations_bitReverse = (int[]) code.get(5);

        transitionTable = new double[transitionTableSize];

        operations_copyFrom = new int[0];
    }

    public void fft(final double[] current) {
        double tempR = 0;
        double tempI = 0;
        double FI = 0;
        double FR = 0;
        double FI2 = 0;
        double FR2 = 0;
        int inL = 0;
        int inR = 0;
        int N = 0;
        int TRANS_L = 0;
        int TRANS_R = 0;
        int i = 0;

        i = 0;// Copy from transition table operations
        while (i < operations_copyFrom.length) {
            inR = operations_copyFrom[i + 1];
            N = operations_copyFrom[i + 2];
            current[inR] = transitionTable[N];
            current[inR + 1] = transitionTable[N + 1];
            i += 5;
            // LOGGER.debug("copy from table L {} R {} N {} TL {} TR {}", inL/2, inR/2, N, TRANS_L/2, TRANS_R/2);
        }

        i = 0; // Execute operations for two elements dft - real values as input
        while (i < operations_twos.length) {
            TRANS_L = operations_twos[i + 3];
            TRANS_R = operations_twos[i + 4];
            inL = operations_twos[i];
            inR = operations_twos[i + 1];
            tempR = current[inR];
            if (TRANS_L >= 0) {
                transitionTable[TRANS_R] = current[inR] = current[inL] - tempR;
                transitionTable[TRANS_R + 1] = current[inR + 1] = 0;
                transitionTable[TRANS_L] = current[inL] = current[inL] + tempR;
                transitionTable[TRANS_L + 1] = current[inL + 1] = 0;
            } else {
                current[inR] = current[inL] - tempR;
                current[inR + 1] = 0;
                current[inL] = current[inL] + tempR;
                current[inL + 1] = 0;
            }
            // LOGGER.debug("merge L {} R {} N {} TL {} TR {}", inL/2, inR/2, N, TRANS_L/2, TRANS_R/2);
            i += 5;
        }

        i = 0; // Execute rest operations
        while (i < operations_rest.length) {
            inL = operations_rest[i];
            inR = operations_rest[i + 1];
            N = operations_rest[i + 2];
            TRANS_L = operations_rest[i + 3];
            TRANS_R = operations_rest[i + 4];

            tempR = current[inR];
            tempI = current[inR + 1];
            FR = FACTOR2[N] * tempR;
            FI = FACTOR3[N] * tempR;
            FR2 = FACTOR3[N] * tempI;
            FI2 = FACTOR2[N] * tempI;
            FR = FR - FR2;
            FI = FI + FI2;

            if (TRANS_L >= 0) {
                transitionTable[TRANS_R] = current[inR] = current[inL] - FR;
                transitionTable[TRANS_R + 1] = current[inR + 1] = current[inL + 1] - FI;
                transitionTable[TRANS_L] = current[inL] = current[inL] + FR;
                transitionTable[TRANS_L + 1] = current[inL + 1] = current[inL + 1] + FI;
            } else {
                current[inR] = current[inL] - FR;
                current[inR + 1] = current[inL + 1] - FI;
                current[inL] = current[inL] + FR;
                current[inL + 1] = current[inL + 1] + FI;
            }
            // LOGGER.debug("merge L {} R {} N {} TL {} TR {}", inL/2, inR/2, N, TRANS_L/2, TRANS_R/2);
            i += 5;
        }

        i = 0; // Bit reverse permutation
        while (i < operations_bitReverse.length) {
            inL = operations_bitReverse[i];
            inR = operations_bitReverse[i + 1];
            tempR = current[inL];
            tempI = current[inL + 1];
            current[inL] = current[inR];
            current[inL + 1] = current[inR + 1];
            current[inR] = tempR;
            current[inR + 1] = tempI;
            i += 5;
        }

        operations_twos = operations_second_twos;
        operations_rest = operations_second_rest;
        operations_copyFrom = operations_copyFrom_origin;
    }

}
