package pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.butterflyoperations;


import pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.butterflyoperations.stages.Radix2ComponentAggregator;
import pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.butterflyoperations.stages.Stage;
import pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.butterflyoperations.timewindow.TimeWindowDecimation;
import pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.butterflyoperations.timewindow.TimeWindowSupplier;
import pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.butterflyoperations.timewindow.TimeWindowSupplierImpl;
import pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.butterflyoperations.transitions.TransitionIdentifier;
import pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.butterflyoperations.transitions.TransitionsManager;

import java.util.List;
import java.util.Map;

public class OperationFactory {

    public static OperationExtender prepareDataForFFT(int windowSize, int offset){
        final TimeWindowSupplier supplier = new TimeWindowSupplierImpl(windowSize, offset);
        final TimeWindowDecimation timeWindowDecimation = new TimeWindowDecimation(supplier);

        final Radix2ComponentAggregator radixAggregator = timeWindowDecimation.radix2();
        final Stage stage = new Stage(radixAggregator);

        final TransitionIdentifier transitionIdentifier = new TransitionIdentifier(stage);
        transitionIdentifier.doIt();
        final TransitionsManager transitionsManager = transitionIdentifier.getTransitionsManager();
        final OperationGenerator operationGenerator = new OperationGenerator(stage);

        Map<Integer, List<Operation>> operations = operationGenerator.generate();

        OperationExtender operationExtender = new OperationExtender();
        operationExtender.extend(operations, transitionsManager);
        return operationExtender;
    }

}
