package pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.butterflyoperations;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.TwiddleFactors;
import pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.butterflyoperations.stages.Stage;

import java.util.*;

public class OperationGenerator {

    private static final Logger LOGGER = LoggerFactory.getLogger(OperationGenerator.class);

    private final Stage stage;

    public OperationGenerator(final Stage stage) {
        this.stage = stage;
    }

    public Map<Integer,List<Operation>> generate() {

        final Map<Integer,List<Operation>> outMap= new TreeMap<>();

        for (Integer stageNr : new TreeSet<Integer>(stage.getMapFirst().keySet())) {
            if(stageNr!=0){
                final List<Operation> list = new ArrayList<>();
                stage.getMapFirst().get(stageNr).stream().forEach(radixComponentCollection->{
                    //LOGGER.debug("Operation stage {} and values: {}", stageNr, radixComponentCollection.getFftData());
                    final List<AbstractMap.SimpleEntry<Integer, Integer>> o = radixComponentCollection.getOdd().get().getFftData();
                    final List<AbstractMap.SimpleEntry<Integer, Integer>> e = radixComponentCollection.getEven().get().getFftData();
                    final Object[] tw = radixComponentCollection.getButterflyParameter_Twiddle();
                    for (int i = 0; i < o.size(); i++) {
                        //LOGGER.debug("Merge O:{} E:{} N:{} Tw:{}", o.get(i).getKey(), e.get(i).getKey(), radixComponentCollection.getButterflyParameter_N(), tw[i]);
                        final Operation operation = new Operation();
                        operation.stage = stageNr;
                        operation.type = Operation.Type.MERGE;
                        operation.op_1 = o.get(i).getKey();
                        operation.op_2 = e.get(i).getKey();
                        operation.op_3 = (TwiddleFactors.REAL_SIZE*radixComponentCollection.getButterflyParameter_N())+(int) tw[i];//radixComponentCollection.getButterflyParameter_N();
                        //operation.op_4 = (int) tw[i];
                        operation.operationPath = radixComponentCollection.getPathToOperation();
                        list.add(operation);
                    }
                });
                outMap.put(stageNr, list);
            }
        }

        return outMap;
    }

}
