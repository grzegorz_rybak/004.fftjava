package pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.butterflyoperations.timewindow;

import pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.butterflyoperations.stages.Radix2Component;
import pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.butterflyoperations.stages.Radix2ComponentAggregator;

import java.util.AbstractMap.SimpleEntry;
import java.util.List;
import java.util.stream.Collectors;

public class TimeWindowDecimation {

    private List<Integer> firstTimeWindow;

    private List<Integer> secondTimeWindow;

    private List<SimpleEntry<Integer, Integer>> firstFFTData;

    private List<SimpleEntry<Integer, Integer>> secondFFTData;

    public TimeWindowDecimation(final TimeWindowSupplier supplier) {
        this.firstTimeWindow = supplier.supplyFirstTimeWindow();
        this.secondTimeWindow = supplier.supplySecondTimeWindow();

        this.firstFFTData = this.firstTimeWindow.stream()
                .map(x -> new SimpleEntry<Integer, Integer>(this.firstTimeWindow.indexOf(x), x))
                .collect(Collectors.toList());

        this.secondFFTData = this.secondTimeWindow.stream()
                .map(x -> new SimpleEntry<Integer, Integer>(this.secondTimeWindow.indexOf(x), x))
                .collect(Collectors.toList());
    }

    public Radix2ComponentAggregator radix2() {
        final Radix2Component firstFFTRadix = new Radix2Component(this.firstFFTData, "").recursiveDivision();
        final Radix2Component secondFFTRadix = new Radix2Component(this.secondFFTData, "").recursiveDivision();

        Radix2ComponentAggregator radix2ComponentAggregator = new Radix2ComponentAggregator();
        radix2ComponentAggregator.radix2ComponentFirst = firstFFTRadix;
        radix2ComponentAggregator.radix2ComponentSecond = secondFFTRadix;
        return radix2ComponentAggregator;
    }

}

