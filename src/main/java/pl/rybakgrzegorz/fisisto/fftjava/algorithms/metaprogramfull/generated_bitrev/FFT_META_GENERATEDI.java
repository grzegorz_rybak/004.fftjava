package pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.generated_bitrev;

import java.util.function.Consumer;

public abstract class FFT_META_GENERATEDI {
    public Consumer<? super double[]> executor;
}
