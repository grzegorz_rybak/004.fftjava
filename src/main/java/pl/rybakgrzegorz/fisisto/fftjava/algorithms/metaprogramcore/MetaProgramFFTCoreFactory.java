package pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramcore;

import pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.butterflyoperations.OperationExtender;
import pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.butterflyoperations.OperationFactory;
import pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.butterflyoperations.OperationSerializerFeedForward;

import java.util.Map;

public class MetaProgramFFTCoreFactory {


    public MetaProgramFFTCore create(final int windowSize, final int offset) {
        final OperationExtender operationExtender = OperationFactory.prepareDataForFFT(windowSize, offset);

        final OperationSerializerFeedForward serializer = new OperationSerializerFeedForward();
        final Map<Integer, Object> code = serializer.serializer(windowSize, operationExtender);
        return new MetaProgramFFTCore(code, operationExtender.getTransitionTableSize());
    }

}
