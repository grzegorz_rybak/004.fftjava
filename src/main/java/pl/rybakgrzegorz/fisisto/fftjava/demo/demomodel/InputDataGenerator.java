package pl.rybakgrzegorz.fisisto.fftjava.demo.demomodel;

import java.util.function.Function;
import java.util.stream.IntStream;

public class InputDataGenerator {

    private static double[] arrayOf(int size) {
        return IntStream.range(0, size).mapToDouble(i -> i).toArray();
    }

    private static double[] arrayOfRandom(int size) {
        double[] array = new double[size];
        for (int i = 0; i < size; i++) {
            array[i] = (Math.random() * 100);
        }
        return array;
    }

    /**
     * Contract: set number of elements. The output is determined wih strategy is chosen.
     */
    public static Function<Integer, double[]>
            GENERATE_ARRAY_STRATEGY = InputDataGenerator::arrayOfRandom;

}
