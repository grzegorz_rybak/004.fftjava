package pl.rybakgrzegorz.fisisto.fftjava.demo.demoalgorithms;

import pl.rybakgrzegorz.fisisto.fftjava.demo.demomodel.DemoOutput;
import pl.rybakgrzegorz.fisisto.fftjava.demo.demomodel.DemoRunnerArgs;

import java.util.AbstractMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

public interface DemoAlgorithms extends Function<Map.Entry<double[], DemoRunnerArgs>, DemoOutput> {

    static long computeSTFT1d(final Consumer<double[]> f, final double[] originTable, final DemoRunnerArgs args) {
        return compute(f, DemoAlgorithms::cutTo1D, originTable, args);
    }

    static <T> long compute(final Consumer<T> f,
                            final BiFunction<double[], Map.Entry<Integer, DemoRunnerArgs>, T> cutFunction,
                            double[] originTable,
                            DemoRunnerArgs args) {
        long start = System.nanoTime();
        for (int i = 0; i < args.sTFTSize; i = i + args.overlap) {
            final T D2_toFFT = (T) cutFunction.apply(originTable, new AbstractMap.SimpleEntry(i, args));
            f.accept(D2_toFFT);
        }
        long stop = System.nanoTime();
        return (stop - start);
    }

    static double[] cutTo1D(double[] originTable, Map.Entry<Integer, DemoRunnerArgs> e) {
        double[] out = new double[e.getValue().wSize];
        for (int i = e.getKey(); i < e.getKey() + e.getValue().wSize; i++) {
            out[i - e.getKey()] = originTable[i];
        }
        return out;
    }

}
