package pl.rybakgrzegorz.fisisto.fftjava.demo;

import pl.rybakgrzegorz.fisisto.fftjava.demo.demoalgorithms.*;
import pl.rybakgrzegorz.fisisto.fftjava.demo.demomodel.DemoRunnerArgs;
import pl.rybakgrzegorz.fisisto.fftjava.demo.demomodel.ExecutionOutput;
import pl.rybakgrzegorz.fisisto.fftjava.demo.demomodel.InputDataGenerator;

import java.util.AbstractMap;

public class Execution {

    public void runSingleIteration(final DemoRunnerArgs demoArguments, final ExecutionOutput executionTimes) {
        final double[] originFullArray = InputDataGenerator.GENERATE_ARRAY_STRATEGY.apply(demoArguments.sTFTSize + demoArguments.wSize);
        final AbstractMap.SimpleEntry entry = new AbstractMap.SimpleEntry(originFullArray, demoArguments);

        switch (demoArguments.algorithm) {
            case 1:
                executionTimes.put("MetaFFT", new AlgorithmMetaProgrammingGeneratedCode().apply(entry));
                executionTimes.put("JTransform", new AlgorithmJTransform().apply(entry));
                executionTimes.put("Columbia", new AlgorithmColumbia().apply(entry));
                executionTimes.put("FFTW", new AlgorithmFFTW().apply(entry));
                executionTimes.put("Recursive", new AlgorithmRecursive().apply(entry));
                executionTimes.put("DFT", new AlgorithmDFT().apply(entry));
                executionTimes.put("SDFT", new AlgorithmSDFT().apply(entry));
                break;
            case 2:
                executionTimes.put("MetaFFT", new AlgorithmMetaProgrammingGeneratedCode().apply(entry));
                executionTimes.put("JTransform", new AlgorithmJTransform().apply(entry));
                executionTimes.put("Columbia", new AlgorithmColumbia().apply(entry));
                executionTimes.put("FFTW", new AlgorithmFFTW().apply(entry));
                executionTimes.put("Recursive", new AlgorithmRecursive().apply(entry));
                executionTimes.put("SDFT", new AlgorithmSDFT().apply(entry));
                break;
            case 3:
                executionTimes.put("MetaFFT", new AlgorithmMetaProgrammingGeneratedCode().apply(entry));
                executionTimes.put("JTransform", new AlgorithmJTransform().apply(entry));
                executionTimes.put("Columbia", new AlgorithmColumbia().apply(entry));
                break;
            case 4:
                executionTimes.put("MetaFFTCore", new AlgorithmMetaProgrammingCore().apply(entry));
                executionTimes.put("MetaFFT", new AlgorithmMetaProgrammingGeneratedCode().apply(entry));
                executionTimes.put("JTransform", new AlgorithmJTransform().apply(entry));
                executionTimes.put("Columbia", new AlgorithmColumbia().apply(entry));
                break;
            case 5:
                executionTimes.put("MetaFFTCore", new AlgorithmMetaProgrammingCore().apply(entry));
                executionTimes.put("MetaFFTCoreStockham", new AlgorithmMetaProgrammingGeneratedCodeStockham().apply(entry));
                executionTimes.put("MetaFFT", new AlgorithmMetaProgrammingGeneratedCode().apply(entry));
                executionTimes.put("JTransform", new AlgorithmJTransform().apply(entry));
                executionTimes.put("Columbia", new AlgorithmColumbia().apply(entry));
                break;
        }

    }

}
