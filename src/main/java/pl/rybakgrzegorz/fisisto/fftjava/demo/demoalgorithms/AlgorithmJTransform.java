package pl.rybakgrzegorz.fisisto.fftjava.demo.demoalgorithms;

import org.jtransforms.fft.DoubleFFT_1D;
import pl.rybakgrzegorz.fisisto.fftjava.demo.demomodel.DemoOutput;
import pl.rybakgrzegorz.fisisto.fftjava.demo.demomodel.DemoRunnerArgs;

import java.util.Map;

/**
 * Algorithm execution wrapper for 'jTransform' STFT.
 * The input array is given in the entry key.
 * DemoAlgorithms.computeSTFT1d method based on variables 'iteration' and 'overlap' from DemoRunnerArgs
 * creates a sub-array for STFT computation.
 */
public class AlgorithmJTransform implements DemoAlgorithms {

    @Override
    public DemoOutput apply(final Map.Entry<double[], DemoRunnerArgs> entry) {
        final DemoOutput demoOutput = new DemoOutput();
        final DoubleFFT_1D jTransform = new DoubleFFT_1D(entry.getValue().wSize);
        final long executionTime = DemoAlgorithms.computeSTFT1d(x -> {
            double[] toCompute2 = new double[x.length * 2];
            for (int i = 0; i < x.length; i++) {
                toCompute2[i * 2] = x[i];
                toCompute2[i * 2 + 1] = 0;
            }
            jTransform.complexForward(toCompute2);
            demoOutput.setData(toCompute2);
        }, entry.getKey(), entry.getValue());
        demoOutput.setTime(executionTime);
        return demoOutput;
    }

}
