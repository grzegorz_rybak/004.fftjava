package pl.rybakgrzegorz.fisisto.fftjava.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.rybakgrzegorz.fisisto.fftjava.demo.demomodel.DemoOutput;
import pl.rybakgrzegorz.fisisto.fftjava.demo.demomodel.ExecutionOutput;

import java.util.List;

public class ExecutionAccuracyAnalyzer {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExecutionAccuracyAnalyzer.class);

    public void analyse(final ExecutionOutput executionOutput) {
        final int elements = executionOutput.values().stream().findFirst().get().get(0).getData().length / 2;

        for (int i = 0; i < elements; i++) {
            int finalI = i;
            executionOutput.keySet().forEach(algorithm->{
                List<DemoOutput> outputs = executionOutput.get(algorithm);
                DemoOutput lastComputationResult = outputs.get(outputs.size() - 1);
                double real = lastComputationResult.getData()[finalI];
                double imag = lastComputationResult.getData()[finalI +1];
                LOGGER.debug("For index {} algorithm: {} [{}|{}]", finalI, algorithm, real,imag);
            });
        }
    }

}
