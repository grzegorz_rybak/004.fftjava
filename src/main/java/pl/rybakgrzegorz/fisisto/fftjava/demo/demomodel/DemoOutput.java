package pl.rybakgrzegorz.fisisto.fftjava.demo.demomodel;

public class DemoOutput {

    private double[] data;

    private long time;

    public double[] getData() {
        return data;
    }

    public void setData(double[] data) {
        this.data = data;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
