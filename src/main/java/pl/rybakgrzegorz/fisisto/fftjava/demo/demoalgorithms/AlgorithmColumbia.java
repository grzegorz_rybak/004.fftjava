package pl.rybakgrzegorz.fisisto.fftjava.demo.demoalgorithms;

import pl.rybakgrzegorz.fisisto.fftjava.algorithms.columbia.ColumbiaFFT;
import pl.rybakgrzegorz.fisisto.fftjava.demo.demomodel.DemoOutput;
import pl.rybakgrzegorz.fisisto.fftjava.demo.demomodel.DemoRunnerArgs;

import java.util.Map;

/**
 * Algorithm execution wrapper for 'Columbia' STFT.
 * The input array is given in the entry key.
 * DemoAlgorithms.computeSTFT1d method based on variables 'iteration' and 'overlap' from DemoRunnerArgs
 * creates a sub-array for STFT computation.
 */
public class AlgorithmColumbia implements DemoAlgorithms {

    @Override
    public DemoOutput apply(final Map.Entry<double[], DemoRunnerArgs> entry) {
        final DemoOutput demoOutput = new DemoOutput();
        final ColumbiaFFT columbiaFFT = new ColumbiaFFT();
        final long time = DemoAlgorithms.computeSTFT1d(x -> {
            final double[] output = columbiaFFT.fft(x);
            demoOutput.setData(output);
        }, entry.getKey(), entry.getValue());
        demoOutput.setTime(time);
        return demoOutput;
    }

}
