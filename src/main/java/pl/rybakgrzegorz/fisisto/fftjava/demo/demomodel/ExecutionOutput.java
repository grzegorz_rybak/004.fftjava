package pl.rybakgrzegorz.fisisto.fftjava.demo.demomodel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExecutionOutput extends HashMap<String, List<DemoOutput>> {

    /**
     * Method to handle a list as value.
     * May be changed to some kind of multimap or sth else.
     */
    public Object put(String key, DemoOutput value) {
        putIfAbsent(key, new ArrayList());
        get(key).add(value);
        return key;
    }
}