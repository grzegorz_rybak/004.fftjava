package pl.rybakgrzegorz.fisisto.fftjava.demo.demoalgorithms;

import org.bytedeco.javacpp.DoublePointer;
import org.bytedeco.javacpp.Loader;
import org.bytedeco.javacpp.fftw3;
import org.jtransforms.fft.DoubleFFT_1D;
import pl.rybakgrzegorz.fisisto.fftjava.algorithms.columbia.ColumbiaFFT;
import pl.rybakgrzegorz.fisisto.fftjava.demo.demomodel.DemoOutput;
import pl.rybakgrzegorz.fisisto.fftjava.demo.demomodel.DemoRunnerArgs;

import java.util.Map;

import static org.bytedeco.javacpp.fftw3.*;

/**
 * Algorithm execution wrapper for 'FFTW with planning' STFT.
 * The input array is given in the entry key.
 * DemoAlgorithms.computeSTFT1d method based on variables 'iteration' and 'overlap' from DemoRunnerArgs
 * creates a sub-array for STFT computation.
 */
public class AlgorithmFFTW implements DemoAlgorithms {

    {
        Loader.load(fftw3.class);
    }

    @Override
    public DemoOutput apply(final Map.Entry<double[], DemoRunnerArgs> entry) {
        final DemoOutput demoOutput = new DemoOutput();
        final int n = entry.getValue().wSize;

        final DoublePointer signal = new DoublePointer(2 * n);
        final DoublePointer result = new DoublePointer(2 * n);

        final long executionTime = DemoAlgorithms.computeSTFT1d(x -> {
            double[] toCompute2 = new double[x.length * 2];
            for (int i = 0; i < x.length; i++) {
                toCompute2[i * 2] = x[i];
                toCompute2[i * 2 + 1] = 0;
            }
            final fftw3.fftw_plan plan = fftw_plan_dft_1d(n, signal, result, FFTW_FORWARD, (int) FFTW_ESTIMATE);

            signal.put(toCompute2);
            fftw_execute(plan);
            result.get(toCompute2);
            fftw_destroy_plan(plan);

            demoOutput.setData(toCompute2);

        }, entry.getKey(), entry.getValue());

        demoOutput.setTime(executionTime);
        return demoOutput;
    }

}
