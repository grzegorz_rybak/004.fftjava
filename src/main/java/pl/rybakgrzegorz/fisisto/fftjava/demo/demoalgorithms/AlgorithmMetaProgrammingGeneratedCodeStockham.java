package pl.rybakgrzegorz.fisisto.fftjava.demo.demoalgorithms;

import pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.FFT_META_GENERATED_STOCKHAM;
import pl.rybakgrzegorz.fisisto.fftjava.algorithms.metaprogramfull.generated_stock.FFT_META_GENERATEDI;
import pl.rybakgrzegorz.fisisto.fftjava.demo.demomodel.DemoOutput;
import pl.rybakgrzegorz.fisisto.fftjava.demo.demomodel.DemoRunnerArgs;

import java.util.Map;

/**
 * Algorithm execution wrapper for 'Meta-programming with generated code' STFT.
 * The input array is given in the entry key.
 * DemoAlgorithms.computeSTFT1d method based on variables 'iteration' and 'overlap' from DemoRunnerArgs
 * creates a sub-array for STFT computation.
 */
public class AlgorithmMetaProgrammingGeneratedCodeStockham implements DemoAlgorithms {

    @Override
    public DemoOutput apply(final Map.Entry<double[], DemoRunnerArgs> entry) {
        final DemoOutput demoOutput = new DemoOutput();
        final FFT_META_GENERATEDI fft_meta_generated = FFT_META_GENERATED_STOCKHAM.createExecutorSupplier(entry.getValue().wSize);
        long executionTime = DemoAlgorithms.computeSTFT1d(x -> {
            double[] toCompute2 = new double[x.length * 2];
            for (int i = 0; i < x.length; i++) {
                toCompute2[i * 2] = x[i];
                toCompute2[i * 2 + 1] = 0;
            }
            fft_meta_generated.executor.accept(toCompute2);
            demoOutput.setData(toCompute2);
        }, entry.getKey(), entry.getValue());
        demoOutput.setTime(executionTime);
        return demoOutput;
    }
}
