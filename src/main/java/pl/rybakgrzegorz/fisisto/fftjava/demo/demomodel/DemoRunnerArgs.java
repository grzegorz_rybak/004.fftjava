package pl.rybakgrzegorz.fisisto.fftjava.demo.demomodel;

import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.spi.BooleanOptionHandler;

public class DemoRunnerArgs {

    @Option(name = "-combinations", handler = BooleanOptionHandler.class, usage = "Ignore 'wSize' parameter and run example combination generator", required = false)
    public boolean useCombinations = false;

    @Option(name = "-printResult", handler = BooleanOptionHandler.class, usage = "Print STFT last window result", required = false)
    public boolean printResult = false;

    @Option(name = "-warmup", usage = "Remove execution time of first iterations", required = false)
    public int warmUp = 0;

    @Option(name = "-iterations", usage = "How many iterations should be done. The time output is an average.", required = false)
    public int iterations = 35;

    @Option(name = "-alg", usage = "The name of the algorithm group", required = false)
    public int algorithm = 3;

    @Option(name = "-overlap", usage = "STFT overlap coefficient", required = false)
    public int overlap = 1;

    @Option(name = "-stft_size", usage = "STFT input array size for one iteration", required = false)
    public int sTFTSize = 10000;

    @Option(name = "-tWindow", usage = "The size of time window", required = false)
    public int wSize = 128;

    @Override
    public String toString() {
        return "\nDemoRunnerArgs{" +
                "\n useCombinations=" + useCombinations +
                ",\n printResult=" + printResult +
                ",\n algorithm=" + algorithm +
                ",\n wSize=" + wSize +
                ",\n overlap=" + overlap +
                ",\n sTFTSize=" + sTFTSize +
                ",\n iterations=" + iterations +
                ",\n warmUp=" + warmUp +
                "\n}";
    }

    public static void validate(DemoRunnerArgs demoRunnerArgs) {
        if (demoRunnerArgs.iterations <= demoRunnerArgs.warmUp) {
            throw new IllegalArgumentException("WarmUp value cannot be the same as iteration count and lower");
        }
    }
}
