package pl.rybakgrzegorz.fisisto.fftjava.demo.demoalgorithms;

import pl.rybakgrzegorz.fisisto.fftjava.algorithms.recursive.Recursive;
import pl.rybakgrzegorz.fisisto.fftjava.demo.demomodel.DemoOutput;
import pl.rybakgrzegorz.fisisto.fftjava.demo.demomodel.DemoRunnerArgs;

import java.util.Map;

/**
 * Algorithm execution wrapper for 'Recursive' STFT.
 * The input array is given in the entry key.
 * DemoAlgorithms.computeSTFT1d method based on variables 'iteration' and 'overlap' from DemoRunnerArgs
 * creates a sub-array for STFT computation.
 */
public class AlgorithmRecursive implements DemoAlgorithms {

    @Override
    public DemoOutput apply(final Map.Entry<double[], DemoRunnerArgs> entry) {
        final DemoOutput demoOutput = new DemoOutput();
        final Recursive recursiveFFT = new Recursive();
        final long time =  DemoAlgorithms.computeSTFT1d(x -> {
            double[][] out = recursiveFFT.compute(x, 0);
            double[] o = new double[out[0].length * 2];
            for (int i = 0; i < out[0].length; i++) {
                o[i*2] = out[0][i];
                o[i*2+1] = out[1][i];
            }
            demoOutput.setData(o);
        }, entry.getKey(), entry.getValue());
        demoOutput.setTime(time);
        return demoOutput;
    }

}

