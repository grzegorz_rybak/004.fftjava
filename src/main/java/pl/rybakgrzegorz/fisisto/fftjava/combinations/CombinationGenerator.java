package pl.rybakgrzegorz.fisisto.fftjava.combinations;

import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Multimap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class CombinationGenerator {

    private static final Logger LOGGER = LoggerFactory.getLogger(CombinationGenerator.class);

    private final Multimap<String, Integer> map = LinkedListMultimap.create();

    public CombinationGenerator addCombination(final String key, final int value) {
        map.put(key, value);
        return this;
    }

    public void combine(final Consumer<List<Integer>> execute) {
        final List<String> list = Arrays.asList((String[]) map.keySet().toArray(new String[]{}));
        combine(0, new ArrayList<>(), execute);
    }

    public void combine(int level, final List o, final Consumer<List<Integer>> execute) {
        final List<String> list = Arrays.asList((String[]) map.keySet().toArray(new String[]{}));

        if (list.size() > level) {
            for (Integer value : map.get(list.get(level))) {
                List<Integer> newList = new ArrayList<>(o);
                newList.add(value);
                combine(level + 1, newList, execute);
            }
        } else {
            LOGGER.debug("Apply Leaf: [{}] -> {}", list, o);
            execute.accept(o);
        }
    }
}
