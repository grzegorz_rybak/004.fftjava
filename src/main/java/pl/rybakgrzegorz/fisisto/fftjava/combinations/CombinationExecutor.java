package pl.rybakgrzegorz.fisisto.fftjava.combinations;

import pl.rybakgrzegorz.fisisto.fftjava.demo.demomodel.DemoRunnerArgs;
import pl.rybakgrzegorz.fisisto.fftjava.demo.demomodel.ExecutionBundle;
import pl.rybakgrzegorz.fisisto.fftjava.demo.demomodel.ExecutionOutput;

import java.util.function.Consumer;

/**
 * Example parameters combination for STFT efficiency test.
 * STFT Demo for consecutive window size provided. (8,16,32,64,128,256,512,1024).
 * The count of FFT computations in one STFT iterations: 100 000.
 * The count of STFT iterations: 35 (only last 30 times are summed).
 */
public class CombinationExecutor {

    private final DemoRunnerArgs demoArguments;

    public CombinationExecutor(DemoRunnerArgs demoArguments) {
        this.demoArguments = demoArguments;
    }

    public void doForEachCombination(final Consumer<ExecutionBundle> configurationConsumer) {
        final CombinationGenerator combinationExample = new CombinationGenerator()
                .addCombination("offset", 1)
                .addCombination("wsize", 8)
                .addCombination("wsize", 16)
                .addCombination("wsize", 32)
                .addCombination("wsize", 64)
                .addCombination("wsize", 128)
                .addCombination("wsize", 256)
                .addCombination("wsize", 512)
                .addCombination("wsize", 1024)
                .addCombination("warm", this.demoArguments.warmUp)
                .addCombination("stft_size", this.demoArguments.sTFTSize)
                .addCombination("algorithm", this.demoArguments.algorithm)
                .addCombination("iterations", this.demoArguments.iterations);

        final CombinationGenerator combinator = combinationExample;
        combinator.combine(array -> {

            final DemoRunnerArgs arguments = new DemoRunnerArgs();
            arguments.printResult = this.demoArguments.printResult;
            arguments.useCombinations = this.demoArguments.useCombinations;
            arguments.overlap = array.get(0);
            arguments.wSize = array.get(1);
            arguments.warmUp = array.get(2);
            arguments.sTFTSize = array.get(3);
            arguments.algorithm = array.get(4);
            arguments.iterations = array.get(5);

            final ExecutionOutput executionOutput = new ExecutionOutput();
            final ExecutionBundle executionBundle = new ExecutionBundle();
            executionBundle.executionOutput = executionOutput;
            executionBundle.demoRunnerArgs = arguments;

            configurationConsumer.accept(executionBundle);
        });
    }
}
