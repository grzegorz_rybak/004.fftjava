java -jar ../../../../target/fftjava-1.0-SNAPSHOT-jar-with-dependencies.jar ^
-warmup 5 ^
-iterations 35 ^
-alg 3 ^
-overlap 1 ^
-stft_size 1000000 ^
-tWindow 8
